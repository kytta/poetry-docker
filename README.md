# poetry-docker

> Docker Image for Poetry based on official Python images

This image is a version of the [official Python images](https://github.com/docker-library/python) with the one addition being the installation of [Poetry](https://python-poetry.org/).

This is not really a production-ready project. Included is only the latest Python version and only the Linux-based images. Alpine 3.12 is also not included. These are not relevant to me, so if you want to contribute — you're welcome!

## Licence

[BSD-3-Clause](https://spdx.org/licenses/BSD-3-Clause.html) © 2021, Nikita Karamov
